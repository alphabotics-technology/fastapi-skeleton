# FastAPI fresh

## Run it

Run the server with:

```
$ uvicorn main:app --reload
```

## Check it

Open your browser at:

http://127.0.0.1:8000
http://127.0.0.1:8000/items/5?q=somequery


You will see the JSON response as:

```json
{"item_id": 5, "q": "somequery"}
```

You already created an API that:

- Receives HTTP requests in the paths / and /items/{item_id}.
- Both paths take GET operations (also known as HTTP methods).
- The path /items/{item_id} has a path parameter item_id that should be an int.
- The path /items/{item_id} has an optional str query parameter q.

## Interactive API docs

Now go to http://127.0.0.1:8000/docs.

You will see the automatic interactive API documentation (provided by Swagger UI):

![](index-01-swagger-ui-simple.png "Interactive API docs")

## Alternative API docs
And now, go to http://127.0.0.1:8000/redoc.

You will see the alternative automatic documentation (provided by ReDoc):

![](index-02-redoc-simple.png "Interactive API docs")
