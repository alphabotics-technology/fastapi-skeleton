# FastAPI Simple CRUD

A simple Restful API with Create, Read, Update, Delete features.

Restful API definition:

- GET: for read one or list record(s).
- POST: for create a new record.
- PATCH: for update a record by fiedls.
- DELETE: for delete a record.

## Run it

Run project with Virtualenv

```
virtualenv -p python3 venv
source venv/bin/activate

pip install -r requirements/base.txt

uvicorn main:app --reload
```

## Test

To test your API open your browser at http://127.0.0.1:8000/docs

